package com.solvegen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)

@XmlRootElement
public class Answer {
    @XmlElement(name="Greeting_message")
    private String message;

    public Answer() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = "Hello, " + message;
    }
}
