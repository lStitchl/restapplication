package com.solvegen;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloAlex extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            JAXBContext jc = JAXBContext.newInstance(Answer.class);
            User userFromRequest = getUserFromRequest(req);
            resp.setContentType("text/xml");
            PrintWriter out = resp.getWriter();
            Answer answer = new Answer();
            answer.setMessage(userFromRequest.getName());
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(answer, out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public User getUserFromRequest(HttpServletRequest request) throws JAXBException, IOException {
        JAXBContext jc = JAXBContext.newInstance(User.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        User user = (User) unmarshaller.unmarshal(request.getInputStream());
        return user;
    }
}
