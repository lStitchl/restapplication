package com.solvegen;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Greating")
public class User {
    @XmlElement(name="Name")
    private String name;

    public String getName() {
        return name;
    }
}
